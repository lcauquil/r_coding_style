---
title: "Coding style"
author: "Laurent Cauquil"
date: "2023-09-07"
format: 
  html:
    toc: true
    echo: fenced
    embed-resources: true
    toc-expand: true
    theme: lux
    keep-md: true
editor: 
  markdown: 
    wrap: 72
execute: 
  eval: false
---



<br><br>
  
Document très largement inspiré de la page [The tidyverse style guide](https://style.tidyverse.org/index.html)

>    “Good coding style is like correct punctuation: you can manage without it, butitsuremakesthingseasiertoread”

# Fichiers

## Nom de fichiers

Les noms doivent être explicites et se terminer par `.R`  
Pas de caractères spéciaux ou d'espaces.  
Utiliser des lettres, des chiffres, `-` et `_` 

    # Good
    fit_models.R
    utility_functions.R

    # Bad
    fit models.R
    foo.r
    stuff.r

Numéroter éventuellement, s'il y a un ordre particulier, en utilisant une numérotation à 2 chiffres minimum

    00_download.R
    01_explore.R
    ...
    09_model.R
    10_visualize.R

En cas d'oubli d'un fichier, il vaut mieux les renommer que de rajouter des `02a` et `02b`,

**ATTENTION à l'utilisation des lettres capitales**,
Windows ne fait pas la différence mais linux oui !

## Structuration interne

Découper le fichier en utilisant des lignes de `-` ou `=`


::: {.cell}

````{.cell-code}
```{{r}}
# Load data ---------------------------

# Plot data ---------------------------
```
````
:::


Charger tous les packages en début de script.  
Eviter les chargement à partir d'un fichier à part (comme le `.Rprofile`)

# Synthaxe

## Nommer les objets


> "There are only two hard things in Computer Science: cache invalidation and 
> naming things." 
>
> --- Phil Karlton

Ne pas utiliser de majuscule pour nommer les variables ou les fonctions.  
Utiliser les lettres, les chiffres et "`_`".  
La méthode snake case: séparer chaque mot par un "`_`".  
**Les noms de variables ne doivent pas commencer par un chiffre.**


::: {.cell}

````{.cell-code}
```{{r}}
# Good
day_one
day_1

# Bad
DayOne
dayone
```
````
:::


Eviter d'entasser des données dans différentes variables du style (`model_2018`, `model_2019`, `model_2020`), utiliser plutôt des listes.

En général, les noms de variables sont des noms et les fonctions des verbes.  
Essayez de trouver des noms concis et significatifs (ce n'est pas facile !).


::: {.cell}

````{.cell-code}
```{{r}}
# Good
day_one

# Bad
first_day_of_the_month
djm1
```
````
:::


Ne pas reprendre des noms déjà utilisés dans R comme fonctions ou variables, c'est source de confusion.


::: {.cell}

````{.cell-code}
```{{r}}
# Bad
T <- FALSE
c <- 10
mean <- function(x) sum(x)
```
````
:::


## Espacements

### Virgules

Toujours rajouter un espace après une virgule, jamais avant.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
x[, 1]

# Bad
x[,1]
x[ ,1]
x[ , 1]
```
````
:::


### Parenthèses

Ne pas mettre d'espace de part et d'autres des parenthèses.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
mean(x, na.rm = TRUE)

# Bad
mean (x, na.rm = TRUE)
mean( x, na.rm = TRUE )
```
````
:::


Mettre un espace avant et après les parenthèses quand elles sont utilisées avec `if`, `for`, ou `while`.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
if (debug) {
  show(x)
}

# Bad
if(debug){
  show(x)
}
```
````
:::


Mettre un espace après `()` utilisé en argument d'une fonction:


::: {.cell}

````{.cell-code}
```{{r}}
# Good
function(x) {}

# Bad
function (x) {}
function(x){}
```
````
:::


### Accolades

Les doubles accolades, `{{ }}`, doivent avoir des espaces intérieurs:


::: {.cell}

````{.cell-code}
```{{r}}
# Good
max_by <- function(data, var, by) {
  data %>%
    group_by({{ by }}) %>%
    summarise(maximum = max({{ var }}, na.rm = TRUE))
}

# Bad
max_by <- function(data, var, by) {
  data %>%
    group_by({{by}}) %>%
    summarise(maximum = max({{var}}, na.rm = TRUE))
}
```
````
:::



### Operateurs

La plupart des opératuers (`==`, `+`, `-`, `<-`, etc.) doivent être encadrés d'un espace:


::: {.cell}

````{.cell-code}
```{{r}}
# Good
height <- (feet * 12) + inches
mean(x, na.rm = TRUE)

# Bad
height<-feet*12+inches
mean(x, na.rm=TRUE)
```
````
:::


Quelques rares exceptions où il ne faut pas d'espace:

*   `::`, `:::`, `$`, `@`, `[`, `[[`, `^`, unary `-`, unary `+`, and `:`.
  

    ::: {.cell}
    
    ````{.cell-code}
    ```{{r}}
    # Good
    sqrt(x^2 + y^2)
    df$z
    x <- 1:10
    
    # Bad
    sqrt(x ^ 2 + y ^ 2)
    df $ z
    x <- 1 : 10
    ```
    ````
    :::

  
* Pour les formules:
  + pas d'espace si RHS unique (partie droite d'une formule)
 

    ::: {.cell}
    
    ````{.cell-code}
    ```{{r}}
    # Good
    ~foo
    tribble(
      ~col1, ~col2,
      "a",   "b"
    )
    
    # Bad
    ~ foo
    tribble(
      ~ col1, ~ col2,
      "a", "b"
    )
    ```
    ````
    :::


  + un espace si RHS complexe


    ::: {.cell}
    
    ````{.cell-code}
    ```{{r}}
    # Good
    ~ .x + .y
    
    # Bad
    ~.x + .y
    ```
    ````
    :::


*   Pas d'espace pour les `!!` (bang-bang) and `!!!` (bang-bang-bang)


    ::: {.cell}
    
    ````{.cell-code}
    ```{{r}}
    # Good
    call(!!xyz)
    
    # Bad
    call(!! xyz)
    call( !! xyz)
    call(! !xyz)
    ```
    ````
    :::


### Espaces supplémentaires

L'ajout d'espaces est permis s'il aide à la lisibilité du code autour des `=` et `<-`. 


::: {.cell}

````{.cell-code}
```{{r}}
# Good
list(
  total = a + b + c,
  mean  = (a + b + c) / n
)
# Also fine
list(
  total = a + b + c,
  mean = (a + b + c) / n
)
```
````
:::


## Appel de fonctions

### Arguments

2 types d'arguments: 

 - ceux qui concernent les données
 - ceux qui détaillent le fonctionnement de la fonction 
 
Généralement, on omet le nom de l'argument qui détermine les données.  
Si les paramètres par défaut sont remplacés, il faut utiliser le nom complet:


::: {.cell}

````{.cell-code}
```{{r}}
# Good
mean(1:10, na.rm = TRUE)

# Bad
mean(x = 1:10, , FALSE)
mean(, TRUE, x = c(1:10, NA))
```
````
:::


### Assignation

Eviter les assignations dans les fonctions:


::: {.cell}

````{.cell-code}
```{{r}}
# Good
x <- complicated_function()
if (nzchar(x) < 1) {
  # do something
}

# Bad
if (nzchar(x <- complicated_function()) < 1) {
  # do something
}
```
````
:::


Exception pour les fontions qui capturent les "side-effects":

C'est le cas lorsque le résultat n'est pas sauvegardé dans un objet mais affiché par la fonction `print()` ou génère un graph.


::: {.cell}

````{.cell-code}
```{{r}}
output <- capture.output(x <- f())
```
````
:::


## Déroulé (flow)

### Blocks de code (indentation)

`{}`:

* `{` doit être le dernier caractère de la ligne. 
  Le code qui l'appelle (e.g., un `if`, une déclaration de fonction,...) doit être sur la même ligne.

* Le contenu doit être indenté de 2 espaces

* `}` doit être le premier caractère de la ligne.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
if (y < 0 && debug) {
  message("y is negative")
}

if (y == 0) {
  if (x > 0) {
    log(x)
  } else {
    message("x is negative or zero")
  }
} else {
  y^x
}

test_that("call1 returns an ordered factor", {
  expect_s3_class(call1(x, y), c("factor", "ordered"))
})

tryCatch(
  {
    x <- scan()
    cat("Total: ", sum(x), "\n", sep = "")
  },
  interrupt = function(e) {
    message("Aborted by user")
  }
)


# Bad
if (y < 0 && debug) {
message("Y is negative")
}

if (y == 0)
{
    if (x > 0) {
      log(x)
    } else {
  message("x is negative or zero")
    }
} else { y ^ x }
```
````
:::


### If

* Si `else` est utilisé, il doit être sur la même ligne que `}`.

* `&` and `|` ne sont jamais utilisé dans un `if` car ils retournent un vecteur. Toujours utiliser `&&` et `||`.
  
* NB: `ifelse(x, a, b)` n'est pas une fonction qui remplace `if (x) a else b`.  
  `ifelse()` est vectorisé (i.e. si `length(x) > 1`, alors `a` et `b` seront recyclés) et "impatient" (i.e. `a` et `b` seront toujours évalués). 
  
On peut réécrire un block `if` simple mais long:
        

::: {.cell}

````{.cell-code}
```{{r}}
if (x > 10) {
  message <- "big"
} else {
  message <- "small"
}
```
````
:::

    
En une seule ligne:
    

::: {.cell}

````{.cell-code}
```{{r}}
message <- if (x > 10) "big" else "small"
```
````
:::


### Se passer des `}`

On peut se passer des `}` si l'opération est simple et tient sur une ligne.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
y <- 10
x <- if (y < 20) "Too low" else "Too high"
```
````
:::


Les fonctions qui agissent sur le déroulement du code (`return()`, `stop()` ou `continue`) doivent être entre `{}`:


::: {.cell}

````{.cell-code}
```{{r}}
# Good
if (y < 0) {
  stop("Y is negative")
}
find_abs <- function(x) {
  if (x > 0) {
    return(x)
  }
  x * -1
}
# Bad
if (y < 0) stop("Y is negative")
if (y < 0)
  stop("Y is negative")
find_abs <- function(x) {
  if (x > 0) return(x)
  x * -1
}
```
````
:::


### Conversion implicite de type

Eviter l'évaluation implicite (e.g. from numeric to logical) dans une opération `if`:


::: {.cell}

````{.cell-code}
```{{r}}
# Good
if (length(x) > 0) {
  # do something
}
# Bad
if (length(x)) {
  # do something
}
```
````
:::


### Fonction `switch()`

* Eviter les opérations basées sur la position, préférer les noms.
* Chaque élément doit avoir sa propre ligne.
* Prévoir une erreur pour les cas non prévu, sauf si la donnée est validée en amont.


::: {.cell}

````{.cell-code}
```{{r}}
# Good 
switch(x, 
  a = ,
  b = 1, 
  c = 2,
  stop("Unknown `x`", call. = FALSE)
)

# Bad
switch(x, a = , b = 1, c = 2)
switch(x, a =, b = 1, c = 2)
switch(y, 1, 2, 3)
```
````
:::


## Lignes longues

Se tenir à 80 caractères max par ligne.  
Si vous manquez de place, c'est qu'il faudrait utiliser une fonction pour une partie du code.

Si une fonction est trop longue pour tenir sur une ligne, utiliser une ligne par argument.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
do_something_very_complicated(
  something = "that",
  requires = many,
  arguments = "some of which may be long"
)

# Bad
do_something_very_complicated("that", requires, many, arguments,
                              "some of which may be long"
                              )
```
````
:::


On tolère de mettre sur la même ligne de petits arguments, même si l'appel de la fonction est sur plusieurs lignes.


::: {.cell}

````{.cell-code}
```{{r}}
map(x, f,
  extra_argument_a = 10,
  extra_argument_b = c(1, 43, 390, 210209)
)
```
````
:::


Vous pouvez égalment mettre plusieurs arguments sur la même ligne s'ils sont liés les uns aux autres (e.g., chaines de caractères dans `paste()` or `stop()`). 
Dans la construction des phrases, faire correspondre si possible, une ligne de code avec une ligne de sortie. 


::: {.cell}

````{.cell-code}
```{{r}}
# Good
paste0(
  "Requirement: ", requires, "\n",
  "Result: ", result, "\n"
)
# Bad
paste0(
  "Requirement: ", requires,
  "\n", "Result: ",
  result, "\n")
```
````
:::


## Point virgules

Ne pas mettre de `;` en fin de ligne ou pour utiliser plusieurs commandes sur la même ligne.

## Assignations

Utiliser `<-`, pas `=`


::: {.cell}

````{.cell-code}
```{{r}}
# Good
x <- 5
# Bad
x = 5
```
````
:::


## Données

### Chaines de caractères

Utiliser `"`, pas `'`. 
Seule exception, si le texte contient déjà des `"` et pas de `'`.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
"Text"
'Text with "quotes"'
'<a href="http://style.tidyverse.org">A link</a>'
# Bad
'Text'
'Text with "double" and \'single\' quotes'
```
````
:::


### Vecteur de logiques

Préciser `TRUE` et `FALSE` plutôt que `T` et `F`.

## Commentaires

Chaque ligne de commentaire commence par un `#` suivi d'un espace

Dans un script d'analyse de données, utiliser les commentaires pour mentionner des résultats et des décisions importants.  
Si le commentaire explique ce que le code fait, c'est que le code n'est pas assez clair et recoder.  
S'il y a plus de commentaires que de code faire du [R Markdown][rmd].

[syntax]: https://rdrr.io/r/base/Syntax.html
[rmd]:    https://rmarkdown.rstudio.com/

# Fonctions

## Nommage

Utiliser des verbes pour nommer les fonctions:


::: {.cell}

````{.cell-code}
```{{r}}
# Good
add_row()
permute()
# Bad
row_adder()
permutation()
```
````
:::


## Définition de fonction longue

Utiliser l'indentation, 2 méthodes (préférer la première):

*   

    ::: {.cell}
    
    ````{.cell-code}
    ```{{r}}
    long_function_name <- function(a = "a long argument",
                                   b = "another argument",
                                   c = "another long argument") {
      # As usual code is indented by two spaces.
    }
    ```
    ````
    :::


*   

    ::: {.cell}
    
    ````{.cell-code}
    ```{{r}}
    long_function_name <- function(
        a = "a long argument",
        b = "another argument",
        c = "another long argument") {
      # As usual code is indented by two spaces.
    }
    ```
    ````
    :::


Dans les 2 cas la dernière `)` et la première `{` doivent être sur la même ligne que l'argument.

Cas des arguments trop longs [short and sweet](https://design.tidyverse.org/def-short.html).

## `return()`

Utiliser seulement dans le cas des retours prématurés dans la fonction.  
Sinon, laisser R retourner la dernière expression évaluée.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
find_abs <- function(x) {
  if (x > 0) {
    return(x)
  }
  x * -1
}
add_two <- function(x, y) {
  x + y
}
# Bad
add_two <- function(x, y) {
  return(x + y)
}
```
````
:::


`return()` a toujours sa propre ligne car il change le déroulement normal d'une fonction.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
find_abs <- function(x) {
  if (x > 0) {
    return(x)
  }
  x * -1
}
# Bad
find_abs <- function(x) {
  if (x > 0) return(x)
  x * -1
}
```
````
:::


Si une fonction est utilisée pour ses "side-effects" (comme un graph ou pour sauver un objet) elle doit retourner le premier argument (data) avec la fonction `invisibly()`. Elle pourra être ainsi reprise dans un pipe.  
Exemple [httr](http://httr.r-lib.org/):


::: {.cell}

````{.cell-code}
```{{r}}
print.url <- function(x, ...) {
  cat("Url: ", build_url(x), "\n", sep = "")
  invisible(x)
}
```
````
:::


## Commentaires

Dans le code, utiliser les commentaires pour expliquer le "pourquoi", pas le "quoi" ni le "comment", sinon c'est que le code n'est pas assez clair. 


::: {.cell}

````{.cell-code}
```{{r}}
# Good
# Objects like data frames are treated as leaves
x <- map_if(x, is_bare_list, recurse)
# Bad
# Recurse only with bare lists
x <- map_if(x, is_bare_list, recurse)
```
````
:::


Faire des phrases en commentaires, mettre un point si 2 phrases sur la même ligne, mais pas en fin de ligne:


::: {.cell}

````{.cell-code}
```{{r}}
# Good
# Objects like data frames are treated as leaves
x <- map_if(x, is_bare_list, recurse)
# Do not use `is.list()`. Objects like data frames must be treated
# as leaves.
x <- map_if(x, is_bare_list, recurse)
# Bad
# objects like data frames are treated as leaves
x <- map_if(x, is_bare_list, recurse)
# Objects like data frames are treated as leaves.
x <- map_if(x, is_bare_list, recurse)
```
````
:::


# Pipes

## Introduction

Eviter d'utiliser un pipe quand:

- On doit utiliser plus d'un objet.
- Des objets intermédiaires pourraient être utilisés par ailleurs

## Whitespace

%>% doit être précédé d'un espace et suivi d'un retour à la ligne avec une indentation de 2 espaces pour les lignes suivantes. Le code est plus lisible et évite d'oublier une étape.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
iris %>%
  group_by(Species) %>%
  summarize_if(is.numeric, mean) %>%
  ungroup() %>%
  gather(measure, value, -Species) %>%
  arrange(value)
# Bad
iris %>% group_by(Species) %>% summarize_all(mean) %>%
ungroup %>% gather(measure, value, -Species) %>%
arrange(value)
```
````
:::


## Longues lignes

Si les arguments d'une fonction dépassent une ligne, les mettre chacun sur leur ligne avec une indentation:


::: {.cell}

````{.cell-code}
```{{r}}
iris %>%
  group_by(Species) %>%
  summarise(
    Sepal.Length = mean(Sepal.Length),
    Sepal.Width = mean(Sepal.Width),
    Species = n_distinct(Species)
  )
```
````
:::


## Pipes très courts

Un pipe avec une seule étape peut rester sur une ligne, mais s'il ne doit pas évoluer (ajouter des pipes), le supprimer et utiliser la fonction normalement.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
iris %>% arrange(Species)
iris %>% 
  arrange(Species)
arrange(iris, Species)
```
````
:::


Il est parfois utile d'inclure un pipe court dans l'argument d'une fonction.  
Vérifier néanmoins la lisibilité du code, sinon ne pas hésiter à déplacer le code en dehors du pipe avec des noms explicites.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
x %>%
  select(a, b, w) %>%
  left_join(y %>% select(a, b, v), by = c("a", "b"))
# Better
x_join <- x %>% select(a, b, w)
y_join <- y %>% select(a, b, v)
left_join(x_join, y_join, by = c("a", "b"))
```
````
:::


## No arguments

Le pipe `%>%` du package magrittr permet de se passer de `()` quand la fonction n'a pas d'argument, mais il faut mieux les rajouter quand même.  
Le pipe `|>` quant à lui impose de mettre les `()`.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
x %>% 
  unique() %>%
  sort()
# Bad
x %>% 
  unique %>%
  sort
```
````
:::


## Assignations

3 formes d'assignations:

*   Nom de variable et contenu sur des lignes séparées:


    ::: {.cell}
    
    ````{.cell-code}
    ```{{r}}
    iris_long <-
      iris %>%
      gather(measure, value, -Species) %>%
      arrange(-value)
    ```
    ````
    :::


*   Nom de variable et contenu sur la même ligne:


    ::: {.cell}
    
    ````{.cell-code}
    ```{{r}}
    iris_long <- iris %>%
      gather(measure, value, -Species) %>%
      arrange(-value)
    ```
    ````
    :::


*   Contenu à la fin d'un pipe suivi de `->`:


    ::: {.cell}
    
    ````{.cell-code}
    ```{{r}}
    iris %>%
      gather(measure, value, -Species) %>%
      arrange(-value) ->
      iris_long
    ```
    ````
    :::

    
La dernière méthode est probablement la plus naturelle à écrire mais pas à lire.  
Quand le nom de la variable est placée en premier, son nom nous donne une indication sur son contenu et sur le code qui suit.

Ne pas utiliser l'opérateur `%<>%` du package magrittr.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
x <- x %>% 
  abs() %>% 
  sort()
  
# Bad
x %<>%
  abs() %>% 
  sort()
```
````
:::


# ggplot2


## Introduction

L'utilisation du `+` pour séparer les layers est similaire au `%>%`.


## Espace

`+` est toujours précédé d'un espace et suivi d'un retour à la ligne, même s'il n'y a que 2 couches.  
Après la 1^re^ étape chaque ligne doit être indentée.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
iris %>%
  filter(Species == "setosa") %>%
  ggplot(aes(x = Sepal.Width, y = Sepal.Length)) +
  geom_point()
# Bad
iris %>%
  filter(Species == "setosa") %>%
  ggplot(aes(x = Sepal.Width, y = Sepal.Length)) +
    geom_point()
# Bad
iris %>%
  filter(Species == "setosa") %>%
  ggplot(aes(x = Sepal.Width, y = Sepal.Length)) + geom_point()
```
````
:::


## Longue lignes

Si la liste des arguments ne tient pas sur une ligne, mettre chaque argument sur sa ligne avec une indentation:


::: {.cell}

````{.cell-code}
```{{r}}
# Good
ggplot(aes(x = Sepal.Width, y = Sepal.Length, color = Species)) +
  geom_point() +
  labs(
    x = "Sepal width, in cm",
    y = "Sepal length, in cm",
    title = "Sepal length vs. width of irises"
  ) 
# Bad
ggplot(aes(x = Sepal.Width, y = Sepal.Length, color = Species)) +
  geom_point() +
  labs(x = "Sepal width, in cm", y = "Sepal length, in cm", title = "Sepal length vs. width of irises") 
```
````
:::


Même si ggplot2 vous permet de manipuler les données à l'intérieur de l'argument `data`, il faut éviter et effectuer les modifications avec des pipes avant de commencer le graphe.


::: {.cell}

````{.cell-code}
```{{r}}
# Good
iris %>%
  filter(Species == "setosa") %>%
  ggplot(aes(x = Sepal.Width, y = Sepal.Length)) +
  geom_point()
# Bad
ggplot(filter(iris, Species == "setosa"), aes(x = Sepal.Width, y = Sepal.Length)) +
  geom_point()
```
````
:::
